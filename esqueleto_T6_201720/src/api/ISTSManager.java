package api;

import java.io.File;

import controller.String;
import model.vo.StopVO;
import model.vo.VOStop;

public interface ISTSManager {

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */

	
	public void loadStopTimes(String Ruta);

	public void loadStops();
	
	public String darViajesDeLaParada(int pStopId );

	public String darParadasCompartidas(int iD1, int iD2);
}
