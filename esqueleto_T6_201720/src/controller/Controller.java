package controller;

import java.io.File;

import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops();		
	}	
	
	public static void loadStopTimes() {
		manager.loadStopTimes("data/stop_times.txt");
	}
	public static void listStops(Integer tripId) throws TripNotFoundException{
		throw new TripNotFoundException();
	}
	public static String darViajesDeLaParada(int pStopId ){
		return manager.darViajesDeLaParada(pStopId);
	}
	public static String darParadasCompartidas(int ID1, int ID2)
	{
		return manager.darParadasCompartidas(ID1,ID2);
			
	}

}
