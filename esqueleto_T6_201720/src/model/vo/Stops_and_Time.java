package model.vo;

public class Stops_and_Time
{
	
	private VOStop parada;
	
	private String horaSalida;
	private String horaLlegada;
	
	public Stops_and_Time (VOStop pParada,String horasalida, String pHora)
	{
		parada=pParada;
		horaSalida=horasalida;
		horaLlegada=pHora;
	}

	public VOStop darParada()
	{
		return parada;
		
	}
	
	public String darHoraSalida()
	{
		return horaSalida;
	}
	public String darHoraLlegada()
	{
		return horaLlegada;
	}
}
