package model.vo;

import model.data_structures.HashTableLP;

/**
 * Representation of a Stop object
 */
public class VOStop {

	private int 	stop_id;
	private Number stop_code;
	private String stop_name;
	private String stop_desc;
	private Number stop_lat;
	private Number stop_lon;
	private String zone_id;
	private String stop_url;
	private Number location_type;
	private String parent_station;
	private HashTableLP<Integer, VOTrip> tablaDeViajes;
	

	public VOStop(int pStopId, Number pStopCode, String pStopName, String pStopDesc, Number pStopLat, Number pStopLon, String pZoneId, String pStopUrl,Number pLocationType, String pParentSation){
		
		stop_id=pStopId;
		stop_code=pStopCode;
		stop_name=pStopName;
		stop_desc=pStopDesc;
		stop_lat=pStopLat;
		stop_lon=pStopLon;
		zone_id=pZoneId;
		stop_url=pStopUrl;
		location_type=pLocationType;
		parent_station=pParentSation;
		tablaDeViajes= new HashTableLP<>();
	}
	/**
	 * @return id - stop's id
	 */
	public int darStopId() {
		
		return stop_id;
	}
	
	public Number darStopCode(){
		
		return stop_code;
	}
	
	public void agregarViaje(VOTrip viaje){
		tablaDeViajes.put(viaje.darTripId(), viaje);
	}
	
	public HashTableLP<Integer, VOTrip> darTablaDeViajes(){
		return tablaDeViajes;
	}
	/**
	 * @return name - stop name
	 */
	public String darStopName() {
	
		return stop_name;
	}
	
	public String darStopDesc(){
		return stop_desc;
	}
	public Number darStopLat(){
		return stop_lat;
	}
	public Number darStopLon(){
		return stop_lon;
	}
	public String darZondeId(){
		return zone_id;
	}
	public String darStopUrl(){
		return stop_url;
	}
	public Number darLocationType(){
		return location_type;
	}
	public String darParentStation(){
		return parent_station;
	}

}
