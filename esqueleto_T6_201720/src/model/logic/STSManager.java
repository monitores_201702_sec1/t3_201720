package model.logic;


import java.io.BufferedReader;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import com.google.gson.Gson;


import model.data_structures.HashTableLP;
import model.data_structures.MergeSort;
import model.data_structures.Queue;

import model.data_structures.Queue;

import model.vo.StopVO;

import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;
import api.ISTSManager;


public class STSManager implements ISTSManager{

	private final static String SEPARADOR= ",";

	
	
	
	private HashTableLP<Integer, VOTrip> trips;
	private HashTableLP<Integer, VOStop> stops;
	 
	/**
	 * Se carga la informacion de los viajes por cada ruta, se tiene un lista cirular con rutas, dentro de las cuales se tienen listas circulares con los respectivos viajes.
	 * No se ordenan los datos. Se espera mejorar para el proyecto
	 */
	public void loadTrips(String tripsFile) {

		
		int contador=0;

		try {

			FileReader fr = new FileReader(new File(tripsFile));
			BufferedReader bf= new BufferedReader(fr);
			String linea = bf.readLine();
			linea= bf.readLine();

			while(linea!=null){

				String[] datos= linea.split(SEPARADOR,10);

				Trip trip= new Trip(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Integer.parseInt(datos[2]),datos[3] , datos[4], Integer.parseInt(datos[5]), Integer.parseInt(datos[6]), Integer.parseInt(datos[7]),Integer.parseInt(datos[8]),Integer.parseInt(datos[9]));
				trips.put(trip.tripID(), trip)  ;
				
				linea=bf.readLine();
			}



		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}




	@Override
	public void loadStops() {
		
		
			
			stops= new HashTableLP<>(3001);
				
				try {
					FileReader fr = new FileReader(new File("data/stops.txt"));
					BufferedReader bf= new BufferedReader(fr);
					String linea = bf.readLine();
					linea= bf.readLine();
					
					while(linea!=null){
						String[] datos= linea.split(SEPARADOR,10);
						
						int numero;
						double numero2;
						if(datos[1].equals(" ")){
							numero=0;
						}
						else numero=Integer.parseInt(datos[1]);
						//System.out.println(Integer.parseInt(datos[0])+"," +numero+","+ datos[2]+","+datos[3]+ ","+Double.parseDouble(datos[4])+","+Double.parseDouble(datos[5])+","+ datos[6]+","+ datos[7]+","+datos[8]+","+datos[9]);
						
						VOStop stop= new VOStop(Integer.parseInt(datos[0]), numero, datos[2], datos[3], Double.parseDouble(datos[4]), Double.parseDouble(datos[5]), datos[6], datos[7], Integer.parseInt(datos[8]), datos[9]);
					    stops.put(stop.darStopId(), stop);                 
					
						
						
						linea=bf.readLine();
					}
					bf.close();
					
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			
			
			
		
}
	
	public void loadStopTimes(String stopTimesFile) {

		loadStops();
		try {
			FileReader fr = new FileReader(new File(stopTimesFile));
			BufferedReader bf= new BufferedReader(fr);
			String linea = bf.readLine();
			linea= bf.readLine();
			
			while(linea!=null){
				String[] datos= linea.split(SEPARADOR,9);
				
				int numero;
				double numero2;
				if(datos[5].equals("")){
					numero=0;
				}else numero=Integer.parseInt(datos[6]);
				
				if(datos[8].equals("")){
					numero2=0;
				}else numero2=Double.parseDouble(datos[8]);
				
				VOStopTimes stopTime= new VOStopTimes(Integer.parseInt(datos[0]), datos[1], datos[2],Integer.parseInt(datos[3]) , Integer.parseInt(datos[4]),numero , Integer.parseInt(datos[6]), Integer.parseInt(datos[7]),numero2);
			    
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				cal.setTime(sdf.parse(stopTime.getArrival_time()));
				
				VOTrip viaje = new VOTrip(stopTime.getTrip_id(),cal.getTime());
				
				stops.get(stopTime.getStop_id()).agregarViaje(viaje);
			
				trips.get(stopTime.getTrip_id).agregarParada(stopTime.getStop_sequence(),new Stops_and_Time(stops.get(stopTime.getStop_id()), stopTime.getDeparture_time(),stopTime.getArrival_time()));
				
				
				linea=bf.readLine();
			}
			
			
			
			 
		   
		  
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public String darViajesDeLaParada(int pStopId ){
		String mensaje="";
		
		
		VOStop parada= stops.get(pStopId);
		
		mensaje+= "Los viajes de la parada "+parada.darStopId()+ " ordenados por tiempo esperado de llegada son: \n";
		
		HashTableLP<Integer,VOTrip > viajesDeLaParada= parada.darTablaDeViajes();
		
		VOTrip[] viajesArray= new VOTrip[viajesDeLaParada.size()];
		
		int i=0;
		for(Integer key: viajesDeLaParada.keys()){
			
			viajesArray[i]= viajesDeLaParada.get(key);
			i++;
			
		}
		MergeSort mergeSort = null;
		mergeSort.mergeSort(viajesArray);
		
		for(int j=0;j<viajesArray.length;j++){
			String[] hora=  viajesArray[j].darTiempoDeLlegada().toString().split(" ");
			
		mensaje+=viajesArray[j].darTripId()+"  :  "+hora[3]+ "\n";
		}
		return mensaje;

	}
		

	public String darParadasCompartidas(int ID1, int ID2)
	{
		String mensaje="La ruta 1 tiene el id:" + ID1 + ". La ruta 2 tiene el id:"+ ID2 + "\n" + "Los ID de las paradas compartidas son:" + "\n";
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Trip viaje1= trips.get(ID1);
		Trip viaje2= trips.get(ID2);
		
		
		HashTableSC<Integer,VOTrip > paradasDelViaje1= viaje1.darTablaDeViajes();
		HashTableSC<Integer,VOTrip > paradasDelViaje2 =viaje2.darTablaDeViajes();
		
		for(Integer key: paradasDelViaje1.keys())
		{
			for(Integer key2: paradasDelViaje2.keys())
			{
				if(paradasDelViaje1.get(key).darParada().darStopID()==paradasDelViaje2.get(key2).darParada().darStopID())
				{
					mensaje+= paradasDelViaje1.get(key).darParada().darStopID() + "\n";
					String horaLLegada1= paradasDelViaje1.get(key).darHoraLlegada();
					String horaSalida1= paradasDelViaje1.get(1).darHoraLlegada();
					
					Date date1a= sdf.parse(horaLLegada1);
					Date date1b= sdf.parse(horaSalida1);
					long tiempo1 = date1b.getTime()-date1b.getTime();
					
					String horaLLegada1= paradasDelViaje1.get(key).darHoraLlegada();
					String horaSalida1= paradasDelViaje1.get(1).darHoraSalida();
					
					Date date1a= sdf.parse(horaLLegada1);
					Date date1b= sdf.parse(horaSalida1);
					long tiempo1 = date1a.getTime()-date1b.getTime();
					
					String horaLLegada2= paradasDelViaje2.get(key).darHoraLlegada();
					String horaSalida2= paradasDelViaje2.get(1).darHoraSalida();
					
					Date date2a= sdf.parse(horaLLegada2);
					Date date2b= sdf.parse(horaSalida2);
					long tiempo2 = date2a.getTime()-date2b.getTime();
					
					mensaje+= "La ruta 1 se demora " + tiempo1+ "en llegar a la parada" + "\n";
					mensaje+= "La ruta 2 se demora " + tiempo2+ "en llegar a la parada" + "\n";
					
					
					
				}
			}
		}
		
		
		
		
	}
	


	

	


}
