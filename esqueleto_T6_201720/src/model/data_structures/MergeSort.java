package model.data_structures;

import model.vo.VOTrip;

public class MergeSort {

	 public static void mergeSort(VOTrip[] inputArray) {
	        int size = inputArray.length;
	        if (size < 2)
	            return;
	        int mid = size / 2;
	        int leftSize = mid;
	        int rightSize = size - mid;
	        VOTrip[] left = new VOTrip[leftSize];
	        VOTrip[] right = new VOTrip[rightSize];
	        for (int i = 0; i < mid; i++) {
	            left[i] = inputArray[i];

	        }
	        for (int i = mid; i < size; i++) {
	            right[i - mid] = inputArray[i];
	        }
	        mergeSort(left);
	        mergeSort(right);
	        merge(left, right, inputArray);
	    }

	    public static void merge(VOTrip[] left, VOTrip[] right, VOTrip[] arr) {
	        int leftSize = left.length;
	        int rightSize = right.length;
	        int i = 0, j = 0, k = 0;
	        while (i < leftSize && j < rightSize) {
	            if (left[i].darTiempoDeLlegada().before(right[j].darTiempoDeLlegada())) {
	                arr[k] = left[i];
	                i++;
	                k++;
	            } else {
	                arr[k] = right[j];
	                k++;
	                j++;
	            }
	        }
	        while (i < leftSize) {
	            arr[k] = left[i];
	            k++;
	            i++;
	        }
	        while (j < rightSize) {
	            arr[k] = right[j];
	            k++;
	            j++;
	        }
	    }
}
